function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for( var i=0; i < 5; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

window.userId = makeid();
window.selfEmotion = null;
window.enemyEmotion = null;

$("#userId").text(userId);

var faceMode = affdex.FaceDetectorMode.LARGE_FACES;
var detector = new affdex.PhotoDetector(faceMode);
detector.detectAllEmotions();
detector.detectAllExpressions();
detector.detectAllEmojis();
detector.detectAllAppearance();
detector.start();

if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
  navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
    video.src = window.URL.createObjectURL(stream);
    video.play();
  });
}
var context = document.getElementById("canvas").getContext('2d');
var video = document.getElementById('video');

detector.addEventListener("onInitializeSuccess", function() {
  console.log("initialized")
  $("#snap").on("click", function() {
    publishChallenge();
    prepareToTakePhoto();
  });
});

detector.addEventListener("onInitializeFailure", function() {
  console.log("sadness")
});

detector.addEventListener("onImageResultsFailure", function (image, timestamp, err_detail) {
  console.log("error", err_detail)
  console.log("image", image)
});

detector.addEventListener("onImageResultsSuccess", function (faces, image, timestamp) {
  console.log("faces", faces)

  var emoji = faces[0].emojis.dominantEmoji;
  $("#imageAnalysis").text("your expression is " + emoji)
  publishEmotions(faces[0].emotions);
  selfEmotion = evaluateDominantEmotion(faces[0].emotions);
})

function evaluateWinner(emotions) {
  // Anger > Happy
  // Happy > Sad
  // Sad > Anger
  if( (selfEmotion === "Anger" && enemyEmotion === "Happy") ||
      (selfEmotion === "Happy" && enemyEmotion === "Sad") ||
      (selfEmotion === "Sad" && enemyEmotion === "Anger") ) {
    $("#verdict").text("You Won! Wooo!");
  } else {
    $("#verdict").text("You Lost! Better Luck Next Time!");
  }
}

function prepareToTakePhoto(msg) {
  if(msg === undefined) {
    i = 3;
    var timeInterval = setInterval(function() {
      $("#imageAnalysis").text("Taking image in " + i);
      if(i === 0) {
        $("#imageAnalysis").text("");
        takePhoto();
        clearInterval(timeInterval);
      }
      i--;
    }, 1000);
  } else {
    if(msg.userId != userId) {
      i = 3;
      var timeInterval = setInterval(function() {
        $("#imageAnalysis").text("Taking image in " + i);
        if(i === 0) {
          $("#imageAnalysis").text("");
          takePhoto();
          clearInterval(timeInterval);
        }
        i--;
      }, 1000);
    }
  }
}

function takePhoto() {
  var startTimestamp = (new Date()).getTime() / 1000;
  context.drawImage(video, 0, 0, 640, 480);
  var imageData = context.getImageData(0, 0, 640, 480);
  var now = (new Date()).getTime() / 1000;
  var deltaTime = now - startTimestamp;
  detector.process(imageData, deltaTime);
}

function setEnemyImage(msg) {
  console.log("attempting to set image", msg);
  if(msg.userId != userId) {
    $("#enemyImageCanvas").putImageData(msg.imageData, 0, 0);
  }
}

function setEnemyEmotion(msg) {
  if(msg.userId != userId) {
    enemyEmotion = evaluateDominantEmotion(msg.emotions);
    $("#enemyEmotion").text("your opponent's emotion is " + enemyEmotion);
  }
}

function scoreAggregator(element, index, array) {
  console.log('a[' + index + '] = ' + element);
}

function evaluateDominantEmotion(emotions) {
  var anger = ["anger", "contempt", "disgust"];
  var happy = ["engagement", "joy", "surprize"];
  var sad = ["fear", "sadness"];

  var angerScore = 0;
  var happyScore = 0;
  var sadScore = 0;
  anger.forEach(function(key) {
    angerScore += emotions[key];
  });

  happy.forEach(function(key) {
    happyScore += emotions[key];
  });

  sad.forEach(function(key) {
    sadScore += emotions[key];
  });

  if(angerScore > happyScore && angerScore > sadScore) {
    return "Anger";
  }
  if(happyScore > angerScore && happyScore > sadScore) {
    return "Happy";
  }
  return "Sad";
}
