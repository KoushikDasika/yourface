window.pubnub = new PubNub({
  subscribeKey: "sub-c-53ccb844-383a-11e7-887b-02ee2ddab7fe",
  publishKey: "pub-c-b2a8f135-2791-453e-b6f7-67764728fd9a",
  ssl: true
})

pubnub.addListener({
  message: function(m) {
    // handle message
    console.log("listener message: ", m);
    var channelName = m.channel; // The channel for which the message belongs
    var channelGroup = m.subscription; // The channel group or wildcard subscription match (if exists)
    var pubTT = m.timetoken; // Publish timetoken
    var msg = m.message; // The Payload
    console.log("message", msg)
    switch(channelName) {
      case "challenge-start":
        console.log("received message, starting challenge", msg);
        prepareToTakePhoto(msg);
        break;
      case "emotions":
        console.log("received message, setting emoji", msg);
        setEnemyEmotion(msg);
        evaluateWinner();
        break;
    }
  }
})

pubnub.subscribe({
  channels: ['emotions', 'challenge-start'],
  withPresence: true // also subscribe to presence instances.
})

function publishEmotions(emotions) {
  pubnub.publish({
    message: {
      emotions: emotions,
      userId: userId,
    },
    channel: 'emotions',
    sendByPost: false, // true to send via post
    storeInHistory: false, //override default storage options
    function (status, response) {
      console.log("publish status response", status, response)
    }
  });
}

function publishChallenge() {
  pubnub.publish({
    message: { userId: userId, },
    channel: 'challenge-start',
    sendByPost: false, // true to send via post
    storeInHistory: false, //override default storage options
    function (status, response) {
      console.log("publish status response", status, response)
    }
  });
}
